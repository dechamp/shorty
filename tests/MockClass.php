<?php

namespace DeChampTest;

class MockClass {
    const PRIVATE_PROP = 'privateProperty';
    const PROTECTED_PROP = 'protectedProperty';
    const PUBLIC_PROP = 'publicProperty';

    private $privateProperty = self::PRIVATE_PROP;
    protected $protectedProperty = self::PROTECTED_PROP;
    public $publicProperty = self::PUBLIC_PROP;
}
