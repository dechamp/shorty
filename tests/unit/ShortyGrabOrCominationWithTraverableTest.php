<?php

namespace DeChampTest;

use ArrayIterator;
use DeChamp\Shorty;
use PHPUnit\Framework\TestCase;

class ShortyGrabOrCominationWithTraverableTest extends TestCase
{
    private $testItemsArray = [];
    private $testTraverableObject;
    private $deepArray;
    private $deepObject;

    const NON_EXISTANT_KEY = "nonExistantKey";
    const HAS_FALSE = 'hasFalse';
    const HAS_ARRAY = 'hasArray';
    const HAS_TRUE = 'hasTrue';
    const HAS_ZERO = 'hasZero';
    const HAS_ONE = 'hasOne';
    const HAS_NULL = 'hasNull';
    const HAS_EMPTY_STRING = 'hasEmptyString';
    const HAS_INT = 'hasInt';
    const HAS_STRING = 'hasString';
    const HAS_OBJECT = 'hasObject';
    const DUMMY_VALUE = 'dummyValue';

    public function __construct()
    {
        parent::__construct();

        $this->testItemsArray[self::HAS_FALSE] = false;
        $this->testItemsArray[self::HAS_TRUE] = true;
        $this->testItemsArray[self::HAS_ZERO] = 0;
        $this->testItemsArray[self::HAS_ONE] = 1;
        $this->testItemsArray[self::HAS_NULL] = null;
        $this->testItemsArray[self::HAS_EMPTY_STRING] = '';
        $this->testItemsArray[self::HAS_STRING] = 'dummy';
        $this->testItemsArray[self::HAS_INT] = 123;
        $this->testItemsArray[self::HAS_ARRAY] = $this->testItemsArray;

        $this->testTraverableObject = new ArrayIterator($this->testItemsArray);

        $this->deepArray = [
            "a" => [
                "b" => [
                    "c" => [
                        "d" => "dummy"
                    ]
                ]
            ]
        ];

        $this->deepObject = (object) $this->deepArray;

        $this->testTraverableObjectDeep = new ArrayIterator($this->deepObject);
    }
   
    public function testWithTraverableGrabOrThrowWillReturnTrueOnFoundKey()
    {
        $this->assertTrue(Shorty::grabOrThrow($this->testTraverableObject, self::HAS_TRUE, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithTraverableGrabOrThrowWillReturnFalseOnFoundKey()
    {
        $this->assertFalse(Shorty::grabOrThrow($this->testTraverableObject, self::HAS_FALSE, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithTraverableGrabOrThrowWillReturnZeroOnFoundKey()
    {
        $this->assertEquals(0, Shorty::grabOrThrow($this->testTraverableObject, self::HAS_ZERO, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithTraverableGrabOrThrowWillReturnOneOnFoundKey()
    {
        $this->assertEquals(1, Shorty::grabOrThrow($this->testTraverableObject, self::HAS_ONE, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithTraverableGrabOrThrowWillReturnNullOnFoundKey()
    {
        $this->assertNull(Shorty::grabOrThrow($this->testTraverableObject, self::HAS_NULL, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithTraverableGrabOrThrowWillReturnEmptyStringOnFoundKey()
    {
        $this->assertEquals("", Shorty::grabOrThrow($this->testTraverableObject, self::HAS_EMPTY_STRING, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithTraverableGrabOrThrowWillReturnStringOnFoundKey()
    {
        $this->assertEquals("dummy", Shorty::grabOrThrow($this->testTraverableObject, self::HAS_STRING, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithTraverableGrabOrThrowWillReturnIntOnFoundKey()
    {
        $this->assertEquals(123, Shorty::grabOrThrow($this->testTraverableObject, self::HAS_INT, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithTraverableGrabOrThrowWillReturnArrayOnFoundKey()
    {
        $expectedResponse = (array) $this->testTraverableObject;
        unset($expectedResponse[self::HAS_ARRAY]);
        unset($expectedResponse[self::HAS_OBJECT]);
        $this->assertEquals($expectedResponse, Shorty::grabOrThrow($this->testTraverableObject, self::HAS_ARRAY, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithTraverableGrabOrThrowWillReturnNullOnFailedKey()
    {
        self::expectExceptionMessage(self::DUMMY_VALUE);
        $this->assertEquals(new \Exception(self::DUMMY_VALUE), Shorty::grabOrThrow($this->testTraverableObject, self::NON_EXISTANT_KEY, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithTraverableGrabOrThrowWillReturnStringOnSubArray()
    {
        $this->assertEquals("dummy", Shorty::grabOrThrow($this->testTraverableObject, self::HAS_ARRAY . "/" . self::HAS_STRING, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithTraverableGrabOrThrowWillReturnStringOnSubObjectDeep()
    {
        $this->assertEquals("dummy", Shorty::grabOrThrow($this->testTraverableObjectDeep, "a/b/c/d", new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithTraverableGrabOrThrowWillThrowOnFailedKeySubObjectDeep()
    {
        self::expectExceptionMessage(self::DUMMY_VALUE);
        $this->assertEquals(new \Exception(self::DUMMY_VALUE), Shorty::grabOrThrow($this->testTraverableObjectDeep, "a/b/c/x", new \Exception(self::DUMMY_VALUE)));
    }
}
