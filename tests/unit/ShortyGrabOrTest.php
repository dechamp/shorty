<?php

namespace DeChampTest;

use DeChamp\Shorty;
use PHPUnit\Framework\TestCase;


class ShortyGrabOrTest extends TestCase
{
    private $testItemsArray = [];
    private $testItemsObject;
    private $deepArray;
    private $deepObject;

    const NON_EXISTANT_KEY = "nonExistantKey";
    const HAS_FALSE = 'hasFalse';
    const HAS_ARRAY = 'hasArray';
    const HAS_TRUE = 'hasTrue';
    const HAS_ZERO = 'hasZero';
    const HAS_ONE = 'hasOne';
    const HAS_NULL = 'hasNull';
    const HAS_EMPTY_STRING = 'hasEmptyString';
    const HAS_INT = 'hasInt';
    const HAS_STRING = 'hasString';
    const HAS_OBJECT = 'hasObject';
    const DUMMY_VALUE = 'dummyValue';

    public function __construct()
    {
        parent::__construct();

        $this->testItemsArray[self::HAS_FALSE] = false;
        $this->testItemsArray[self::HAS_TRUE] = true;
        $this->testItemsArray[self::HAS_ZERO] = 0;
        $this->testItemsArray[self::HAS_ONE] = 1;
        $this->testItemsArray[self::HAS_NULL] = null;
        $this->testItemsArray[self::HAS_EMPTY_STRING] = '';
        $this->testItemsArray[self::HAS_STRING] = 'dummy';
        $this->testItemsArray[self::HAS_INT] = 123;
        $this->testItemsArray[self::HAS_ARRAY] = $this->testItemsArray;

        $this->testItemsObject = (object) $this->testItemsArray;
        $this->testItemsObject->{self::HAS_OBJECT} = (object) $this->testItemsArray;

        $this->deepArray = [
            "a" => [
                "b" => [
                    "c" => [
                        "d" => "dummy"
                    ]
                ]
            ]
        ];

        $this->deepObject = (object) $this->deepArray;
    }

    public function testWithArrayGrabOrWillReturnTrueOnFoundKey()
    {
        $this->assertTrue(Shorty::grabOr($this->testItemsArray, self::HAS_TRUE, self::DUMMY_VALUE));
    }

    public function testWithArrayGrabOrWillReturnFalseOnFoundKey()
    {
        $this->assertFalse(Shorty::grabOr($this->testItemsArray, self::HAS_FALSE, self::DUMMY_VALUE));
    }

    public function testWithArrayGrabOrWillReturnZeroOnFoundKey()
    {
        $this->assertEquals(0, Shorty::grabOr($this->testItemsArray, self::HAS_ZERO, self::DUMMY_VALUE));
    }

    public function testWithArrayGrabOrWillReturnOneOnFoundKey()
    {
        $this->assertEquals(1, Shorty::grabOr($this->testItemsArray, self::HAS_ONE, self::DUMMY_VALUE));
    }

    public function testWithArrayGrabOrWillReturnNullOnFoundKey()
    {
        $this->assertNull(Shorty::grabOr($this->testItemsArray, self::HAS_NULL, self::DUMMY_VALUE));
    }

    public function testWithArrayGrabOrWillReturnEmptyStringOnFoundKey()
    {
        $this->assertEquals("", Shorty::grabOr($this->testItemsArray, self::HAS_EMPTY_STRING, self::DUMMY_VALUE));
    }

    public function testWithArrayGrabOrWillReturnStringOnFoundKey()
    {
        $this->assertEquals("dummy", Shorty::grabOr($this->testItemsArray, self::HAS_STRING, self::DUMMY_VALUE));
    }

    public function testWithArrayGrabOrWillReturnIntOnFoundKey()
    {
        $this->assertEquals(123, Shorty::grabOr($this->testItemsArray, self::HAS_INT, self::DUMMY_VALUE));
    }

    public function testWithArrayGrabOrWillReturnArrayOnFoundKey()
    {
        $expectedResponse = $this->testItemsArray;
        unset($expectedResponse[self::HAS_ARRAY]);
        $this->assertEquals($expectedResponse, Shorty::grabOr($this->testItemsArray, self::HAS_ARRAY, self::DUMMY_VALUE));
    }

    public function testWithArrayGrabOrWillReturnNullOnFailedKey()
    {
        $this->assertEquals(self::DUMMY_VALUE, Shorty::grabOr($this->testItemsArray, self::NON_EXISTANT_KEY, self::DUMMY_VALUE));
    }

    public function testWithArrayGrabOrWillReturnStringOnSubArray()
    {
        $this->assertEquals("dummy", Shorty::grabOr($this->testItemsArray, self::HAS_ARRAY . "/" . self::HAS_STRING, self::DUMMY_VALUE));
    }

    public function testWithArrayGrabOrWillReturnStringOnSubArrayDeep()
    {
        $this->assertEquals("dummy", Shorty::grabOr($this->deepArray, "a/b/c/d", self::DUMMY_VALUE));
    }

    public function testWithArrayGrabOrWillReturnNullOnFailedKeySubArrayDeep()
    {
        $this->assertEquals(self::DUMMY_VALUE, Shorty::grabOr($this->deepArray, "a/b/c/x", self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnTrueOnFoundKey()
    {
        $this->assertTrue(Shorty::grabOr($this->testItemsObject, self::HAS_TRUE, self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnFalseOnFoundKey()
    {
        $this->assertFalse(Shorty::grabOr($this->testItemsObject, self::HAS_FALSE, self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnZeroOnFoundKey()
    {
        $this->assertEquals(0, Shorty::grabOr($this->testItemsObject, self::HAS_ZERO, self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnOneOnFoundKey()
    {
        $this->assertEquals(1, Shorty::grabOr($this->testItemsObject, self::HAS_ONE, self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnNullOnFoundKey()
    {
        $this->assertNull(Shorty::grabOr($this->testItemsObject, self::HAS_NULL, self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnEmptyStringOnFoundKey()
    {
        $this->assertEquals("", Shorty::grabOr($this->testItemsObject, self::HAS_EMPTY_STRING, self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnStringOnFoundKey()
    {
        $this->assertEquals("dummy", Shorty::grabOr($this->testItemsObject, self::HAS_STRING, self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnIntOnFoundKey()
    {
        $this->assertEquals(123, Shorty::grabOr($this->testItemsObject, self::HAS_INT, self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnArrayOnFoundKey()
    {
        $expectedResponse = (array) $this->testItemsObject;
        unset($expectedResponse[self::HAS_ARRAY]);
        unset($expectedResponse[self::HAS_OBJECT]);
        $this->assertEquals($expectedResponse, Shorty::grabOr($this->testItemsObject, self::HAS_ARRAY, self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnNullOnFailedKey()
    {
        $this->assertEquals(self::DUMMY_VALUE, Shorty::grabOr($this->testItemsObject, self::NON_EXISTANT_KEY, self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnStringOnSubArray()
    {
        $this->assertEquals("dummy", Shorty::grabOr($this->testItemsObject, self::HAS_ARRAY . "/" . self::HAS_STRING, self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnStringOnSubObjectDeep()
    {
        $this->assertEquals("dummy", Shorty::grabOr($this->deepObject, "a/b/c/d", self::DUMMY_VALUE));
    }

    public function testWithObjectGrabOrWillReturnNullOnFailedKeySubObjectDeep()
    {
        $this->assertEquals(self::DUMMY_VALUE, Shorty::grabOr($this->deepObject, "a/b/c/x", self::DUMMY_VALUE));
    }

    public function testGrabOrWithObjectWillReturnNullOnPrivateProperty()
    {
        self::assertEquals(self::DUMMY_VALUE, Shorty::grabOr(new MockClass(), MockClass::PRIVATE_PROP, self::DUMMY_VALUE));
    }

    public function testGrabOrWithObjectWillReturnNullOnProtectedProperty()
    {
        self::assertEquals(self::DUMMY_VALUE, Shorty::grabOr(new MockClass(), MockClass::PROTECTED_PROP, self::DUMMY_VALUE));
    }

    public function testGrabOrWithObjectWillReturnValueOnPublicProperty()
    {
        self::assertEquals(MockClass::PUBLIC_PROP, Shorty::grabOr(new MockClass(), MockClass::PUBLIC_PROP, self::DUMMY_VALUE));
    }
}
