<?php

namespace DeChampTest;

use DeChamp\Shorty;
use DeChampTest\MockClass;
use PHPUnit\Framework\TestCase;


class ShortyGrabOrNullTest extends TestCase
{
    private $testItemsArray = [];
    private $testItemsObject;
    private $deepArray;
    private $deepObject;

    const NON_EXISTANT_KEY = "nonExistantKey";
    const HAS_FALSE = 'hasFalse';
    const HAS_ARRAY = 'hasArray';
    const HAS_TRUE = 'hasTrue';
    const HAS_ZERO = 'hasZero';
    const HAS_ONE = 'hasOne';
    const HAS_NULL = 'hasNull';
    const HAS_EMPTY_STRING = 'hasEmptyString';
    const HAS_INT = 'hasInt';
    const HAS_STRING = 'hasString';
    const HAS_OBJECT = 'hasObject';

    public function __construct()
    {
        parent::__construct();

        $this->testItemsArray[self::HAS_FALSE] = false;
        $this->testItemsArray[self::HAS_TRUE] = true;
        $this->testItemsArray[self::HAS_ZERO] = 0;
        $this->testItemsArray[self::HAS_ONE] = 1;
        $this->testItemsArray[self::HAS_NULL] = null;
        $this->testItemsArray[self::HAS_EMPTY_STRING] = '';
        $this->testItemsArray[self::HAS_STRING] = 'dummy';
        $this->testItemsArray[self::HAS_INT] = 123;
        $this->testItemsArray[self::HAS_ARRAY] = $this->testItemsArray;

        $this->testItemsObject = (object) $this->testItemsArray;
        $this->testItemsObject->{self::HAS_OBJECT} = (object) $this->testItemsArray;

        $this->deepArray = [
            "a" => [
                "b" => [
                    "c" => [
                        "d" => "dummy"
                    ]
                ]
            ]
        ];

        $this->deepObject = (object) $this->deepArray;
    }

    public function testWithArrayGrabOrNullWillReturnTrueOnFoundKey()
    {
        self::assertTrue(Shorty::grabOrNull($this->testItemsArray, self::HAS_TRUE));
    }

    public function testWithArrayGrabOrNullWillReturnFalseOnFoundKey()
    {
        self::assertFalse(Shorty::grabOrNull($this->testItemsArray, self::HAS_FALSE));
    }

    public function testWithArrayGrabOrNullWillReturnZeroOnFoundKey()
    {
        self::assertEquals(0, Shorty::grabOrNull($this->testItemsArray, self::HAS_ZERO));
    }

    public function testWithArrayGrabOrNullWillReturnOneOnFoundKey()
    {
        self::assertEquals(1, Shorty::grabOrNull($this->testItemsArray, self::HAS_ONE));
    }

    public function testWithArrayGrabOrNullWillReturnNullOnFoundKey()
    {
        self::assertNull(Shorty::grabOrNull($this->testItemsArray, self::HAS_NULL));
    }

    public function testWithArrayGrabOrNullWillReturnEmptyStringOnFoundKey()
    {
        self::assertEquals("", Shorty::grabOrNull($this->testItemsArray, self::HAS_EMPTY_STRING));
    }

    public function testWithArrayGrabOrNullWillReturnStringOnFoundKey()
    {
        self::assertEquals("dummy", Shorty::grabOrNull($this->testItemsArray, self::HAS_STRING));
    }

    public function testWithArrayGrabOrNullWillReturnIntOnFoundKey()
    {
        self::assertEquals(123, Shorty::grabOrNull($this->testItemsArray, self::HAS_INT));
    }

    public function testWithArrayGrabOrNullWillReturnArrayOnFoundKey()
    {
        $expectedResponse = $this->testItemsArray;
        unset($expectedResponse[self::HAS_ARRAY]);
        self::assertEquals($expectedResponse, Shorty::grabOrNull($this->testItemsArray, self::HAS_ARRAY));
    }

    public function testWithArrayGrabOrNullWillReturnNullOnFailedKey()
    {
        self::assertNull(Shorty::grabOrNull($this->testItemsArray, self::NON_EXISTANT_KEY));
    }

    public function testWithArrayGrabOrNullWillReturnStringOnSubArray()
    {
        self::assertEquals("dummy", Shorty::grabOrNull($this->testItemsArray, self::HAS_ARRAY . "/" . self::HAS_STRING));
    }

    public function testWithArrayGrabOrNullWillReturnStringOnSubArrayDeep()
    {
        self::assertEquals("dummy", Shorty::grabOrNull($this->deepArray, "a/b/c/d"));
    }

    public function testWithArrayGrabOrNullWillReturnNullOnFailedKeySubArrayDeep()
    {
        self::assertNull(Shorty::grabOrNull($this->deepArray, "a/b/c/x"));
    }

    public function testGrabOrNullWithObjectWillReturnTrueOnFoundKey()
    {
        self::assertTrue(Shorty::grabOrNull($this->testItemsObject, self::HAS_TRUE));
    }

    public function testGrabOrNullWithObjectWillReturnFalseOnFoundKey()
    {
        self::assertFalse(Shorty::grabOrNull($this->testItemsObject, self::HAS_FALSE));
    }

    public function testGrabOrNullWithObjectWillReturnZeroOnFoundKey()
    {
        self::assertEquals(0, Shorty::grabOrNull($this->testItemsObject, self::HAS_ZERO));
    }

    public function testGrabOrNullWithObjectWillReturnOneOnFoundKey()
    {
        self::assertEquals(1, Shorty::grabOrNull($this->testItemsObject, self::HAS_ONE));
    }

    public function testGrabOrNullWithObjectWillReturnNullOnFoundKey()
    {
        self::assertNull(Shorty::grabOrNull($this->testItemsObject, self::HAS_NULL));
    }

    public function testGrabOrNullWithObjectWillReturnEmptyStringOnFoundKey()
    {
        self::assertEquals("", Shorty::grabOrNull($this->testItemsObject, self::HAS_EMPTY_STRING));
    }

    public function testGrabOrNullWithObjectWillReturnStringOnFoundKey()
    {
        self::assertEquals("dummy", Shorty::grabOrNull($this->testItemsObject, self::HAS_STRING));
    }

    public function testGrabOrNullWithObjectWillReturnIntOnFoundKey()
    {
        self::assertEquals(123, Shorty::grabOrNull($this->testItemsObject, self::HAS_INT));
    }

    public function testGrabOrNullWithObjectWillReturnArrayOnFoundKey()
    {
        $expectedResponse = (array) $this->testItemsObject;
        unset($expectedResponse[self::HAS_ARRAY]);
        unset($expectedResponse[self::HAS_OBJECT]);
        self::assertEquals($expectedResponse, Shorty::grabOrNull($this->testItemsObject, self::HAS_ARRAY));
    }

    public function testGrabOrNullWithObjectWillReturnNullOnFailedKey()
    {
        self::assertNull(Shorty::grabOrNull($this->testItemsObject, self::NON_EXISTANT_KEY));
    }

    public function testGrabOrNullWithObjectWillReturnStringOnSubArray()
    {
        self::assertEquals("dummy", Shorty::grabOrNull($this->testItemsObject, self::HAS_ARRAY . "/" . self::HAS_STRING));
    }

    public function testGrabOrNullWithObjectWillReturnStringOnSubObjectDeep()
    {
        self::assertEquals("dummy", Shorty::grabOrNull($this->deepObject, "a/b/c/d"));
    }

    public function testGrabOrNullWithObjectWillReturnNullOnFailedKeySubObjectDeep()
    {
        self::assertNull(Shorty::grabOrNull($this->deepObject, "a/b/c/x"));
    }
    
    public function testGrabOrNullWithObjectWillReturnNullOnPrivateProperty()
    {
        self::assertNull(Shorty::grabOrNull(new MockClass(), MockClass::PRIVATE_PROP));
    }

    public function testGrabOrNullWithObjectWillReturnNullOnProtectedProperty()
    {
        self::assertNull(Shorty::grabOrNull(new MockClass(), MockClass::PROTECTED_PROP));
    }

    public function testGrabOrNullWithObjectWillReturnValueOnPublicProperty()
    {
        self::assertEquals(MockClass::PUBLIC_PROP, Shorty::grabOrNull(new MockClass(), MockClass::PUBLIC_PROP));
    }
}
