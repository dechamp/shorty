<?php

namespace DeChampTest;

use DeChamp\Shorty;
use PHPUnit\Framework\TestCase;


class ShortygrabOrThrowTest extends TestCase
{
    private $testItemsArray = [];
    private $testItemsObject;
    private $deepArray;
    private $deepObject;

    const NON_EXISTANT_KEY = "nonExistantKey";
    const HAS_FALSE = 'hasFalse';
    const HAS_ARRAY = 'hasArray';
    const HAS_TRUE = 'hasTrue';
    const HAS_ZERO = 'hasZero';
    const HAS_ONE = 'hasOne';
    const HAS_NULL = 'hasNull';
    const HAS_EMPTY_STRING = 'hasEmptyString';
    const HAS_INT = 'hasInt';
    const HAS_STRING = 'hasString';
    const HAS_OBJECT = 'hasObject';
    const DUMMY_VALUE = 'dummyValue';

    public function __construct()
    {
        parent::__construct();

        $this->testItemsArray[self::HAS_FALSE] = false;
        $this->testItemsArray[self::HAS_TRUE] = true;
        $this->testItemsArray[self::HAS_ZERO] = 0;
        $this->testItemsArray[self::HAS_ONE] = 1;
        $this->testItemsArray[self::HAS_NULL] = null;
        $this->testItemsArray[self::HAS_EMPTY_STRING] = '';
        $this->testItemsArray[self::HAS_STRING] = 'dummy';
        $this->testItemsArray[self::HAS_INT] = 123;
        $this->testItemsArray[self::HAS_ARRAY] = $this->testItemsArray;

        $this->testItemsObject = (object) $this->testItemsArray;
        $this->testItemsObject->{self::HAS_OBJECT} = (object) $this->testItemsArray;

        $this->deepArray = [
            "a" => [
                "b" => [
                    "c" => [
                        "d" => "dummy"
                    ]
                ]
            ]
        ];

        $this->deepObject = (object) $this->deepArray;
    }

    public function testWithArraygrabOrThrowWillReturnTrueOnFoundKey()
    {
        $this->assertTrue(Shorty::grabOrThrow($this->testItemsArray, self::HAS_TRUE, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithArraygrabOrThrowWillReturnFalseOnFoundKey()
    {
        $this->assertFalse(Shorty::grabOrThrow($this->testItemsArray, self::HAS_FALSE, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithArraygrabOrThrowWillReturnZeroOnFoundKey()
    {
        $this->assertEquals(0, Shorty::grabOrThrow($this->testItemsArray, self::HAS_ZERO, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithArraygrabOrThrowWillReturnOneOnFoundKey()
    {
        $this->assertEquals(1, Shorty::grabOrThrow($this->testItemsArray, self::HAS_ONE, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithArraygrabOrThrowWillReturnNullOnFoundKey()
    {
        $this->assertNull(Shorty::grabOrThrow($this->testItemsArray, self::HAS_NULL, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithArraygrabOrThrowWillReturnEmptyStringOnFoundKey()
    {
        $this->assertEquals("", Shorty::grabOrThrow($this->testItemsArray, self::HAS_EMPTY_STRING, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithArraygrabOrThrowWillReturnStringOnFoundKey()
    {
        $this->assertEquals("dummy", Shorty::grabOrThrow($this->testItemsArray, self::HAS_STRING, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithArraygrabOrThrowWillReturnIntOnFoundKey()
    {
        $this->assertEquals(123, Shorty::grabOrThrow($this->testItemsArray, self::HAS_INT, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithArraygrabOrThrowWillReturnArrayOnFoundKey()
    {
        $expectedResponse = $this->testItemsArray;
        unset($expectedResponse[self::HAS_ARRAY]);
        $this->assertEquals($expectedResponse, Shorty::grabOrThrow($this->testItemsArray, self::HAS_ARRAY, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithArraygrabOrThrowWillReturnNullOnFailedKey()
    {
        self::expectExceptionMessage(self::DUMMY_VALUE);
        $this->assertEquals(new \Exception(self::DUMMY_VALUE), Shorty::grabOrThrow($this->testItemsArray, self::NON_EXISTANT_KEY, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithArraygrabOrThrowWillReturnStringOnSubArray()
    {
        $this->assertEquals("dummy", Shorty::grabOrThrow($this->testItemsArray, self::HAS_ARRAY . "/" . self::HAS_STRING, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithArraygrabOrThrowWillReturnStringOnSubArrayDeep()
    {
        $this->assertEquals("dummy", Shorty::grabOrThrow($this->deepArray, "a/b/c/d", new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithArraygrabOrThrowWillThrowOnFailedKeySubArrayDeep()
    {
        self::expectExceptionMessage(self::DUMMY_VALUE);
        $this->assertEquals(new \Exception(self::DUMMY_VALUE), Shorty::grabOrThrow($this->deepArray, "a/b/c/x", new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillReturnTrueOnFoundKey()
    {
        $this->assertTrue(Shorty::grabOrThrow($this->testItemsObject, self::HAS_TRUE, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillReturnFalseOnFoundKey()
    {
        $this->assertFalse(Shorty::grabOrThrow($this->testItemsObject, self::HAS_FALSE, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillReturnZeroOnFoundKey()
    {
        $this->assertEquals(0, Shorty::grabOrThrow($this->testItemsObject, self::HAS_ZERO, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillReturnOneOnFoundKey()
    {
        $this->assertEquals(1, Shorty::grabOrThrow($this->testItemsObject, self::HAS_ONE, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillReturnNullOnFoundKey()
    {
        $this->assertNull(Shorty::grabOrThrow($this->testItemsObject, self::HAS_NULL, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillReturnEmptyStringOnFoundKey()
    {
        $this->assertEquals("", Shorty::grabOrThrow($this->testItemsObject, self::HAS_EMPTY_STRING, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillReturnStringOnFoundKey()
    {
        $this->assertEquals("dummy", Shorty::grabOrThrow($this->testItemsObject, self::HAS_STRING, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillReturnIntOnFoundKey()
    {
        $this->assertEquals(123, Shorty::grabOrThrow($this->testItemsObject, self::HAS_INT, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillReturnArrayOnFoundKey()
    {
        $expectedResponse = (array) $this->testItemsObject;
        unset($expectedResponse[self::HAS_ARRAY]);
        unset($expectedResponse[self::HAS_OBJECT]);
        $this->assertEquals($expectedResponse, Shorty::grabOrThrow($this->testItemsObject, self::HAS_ARRAY, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillReturnNullOnFailedKey()
    {
        self::expectExceptionMessage(self::DUMMY_VALUE);
        $this->assertEquals(new \Exception(self::DUMMY_VALUE), Shorty::grabOrThrow($this->testItemsObject, self::NON_EXISTANT_KEY, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillReturnStringOnSubArray()
    {
        $this->assertEquals("dummy", Shorty::grabOrThrow($this->testItemsObject, self::HAS_ARRAY . "/" . self::HAS_STRING, new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillReturnStringOnSubObjectDeep()
    {
        $this->assertEquals("dummy", Shorty::grabOrThrow($this->deepObject, "a/b/c/d", new \Exception(self::DUMMY_VALUE)));
    }

    public function testWithObjectgrabOrThrowWillThrowOnFailedKeySubObjectDeep()
    {
        self::expectExceptionMessage(self::DUMMY_VALUE);
        $this->assertEquals(new \Exception(self::DUMMY_VALUE), Shorty::grabOrThrow($this->deepObject, "a/b/c/x", new \Exception(self::DUMMY_VALUE)));
    }

    public function testgrabOrThrowWithObjectWillThrowOnPrivateProperty()
    {
        self::expectExceptionMessage(self::DUMMY_VALUE);
        self::assertEquals(new \Exception(self::DUMMY_VALUE), Shorty::grabOrThrow(new MockClass(), MockClass::PRIVATE_PROP, new \Exception(self::DUMMY_VALUE)));
    }

    public function testgrabOrThrowWithObjectWillThrowOnProtectedProperty()
    {
        self::expectExceptionMessage(self::DUMMY_VALUE);
        self::assertEquals(new \Exception(self::DUMMY_VALUE), Shorty::grabOrThrow(new MockClass(), MockClass::PROTECTED_PROP, new \Exception(self::DUMMY_VALUE)));
    }

    public function testgrabOrThrowWithObjectWillReturnValueOnPublicProperty()
    {
        self::assertEquals(MockClass::PUBLIC_PROP, Shorty::grabOrThrow(new MockClass(), MockClass::PUBLIC_PROP, new \Exception(self::DUMMY_VALUE)));
    }
}
