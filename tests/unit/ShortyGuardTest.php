<?php

namespace DeChampTest;

use DeChamp\Guard;
use DeChamp\Shorty;
use Exception;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;


class ShortyGuardTest extends TestCase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function testGuardThrowsErrorOnNoCases()
    {
        self::expectException(Exception::class);
        self::expectExceptionMessage(Guard::ERR_MSG_MISSING_CASES);

        Shorty::guard()->allOrThrow('doh!');
    }

    public function testGuardThrowsErrorOnInvalidCaseResponse()
    {
        self::expectException(Exception::class);
        self::expectExceptionMessage(Guard::ERR_MSG_INVALID_CASE_RESPONSE_ERROR);

        Shorty::guard()
            ->addCase(false, '')
            ->allOrThrow('doh!');
    }

    public function testGuardThrowsErrorOnInvalidTestReturnType()
    {
        $test = 'string';

        self::expectException(Exception::class);
        self::expectExceptionMessage(sprintf(
            Guard::ERR_MSG_INVALID_TEST_RETURN_TYPE,
            $test,
            gettype($test)
        ));

        Shorty::guard()
            ->addCase($test, 'this throws an error')
            ->allOrThrow('doh!');
    }

    public function testAllOrThrowWillThrowWhenOneFails()
    {
        $passingCase = 'I should not case failure';
        $failedCase  = 'I should be triggered';

        self::expectException(Exception::class);
        self::expectExceptionMessage($failedCase);

        Shorty::guard()
            ->addCase(true, $passingCase)
            ->addCase(false, $failedCase)
            ->allOrThrow(InvalidArgumentException::class);
    }

    public function testAllOrThrowWillVoidWhenNoneFail()
    {
        $falseValue = false;
        $emptyString = '';
        $zero = 0;

        Shorty::guard()
            ->addCase(true, 'true')
            ->addCase(new \stdClass() instanceof \stdClass, 'instance check')
            ->addCase(isset($emptyString), 'empty strings are going to pass')
            ->addCase(isset($zero), 'zero will pass as only false will fail')
            ->addCase(($falseValue === false), 'expecting false')
            ->addCase((5 < 10), 'Five is less then 10')
            ->addCase(function () {
                return true;
            }, 'callback should of return true')
            ->allOrThrow(InvalidArgumentException::class);
    }

    public function testAllOrThrowWillThrowWhenOneItemFailsWithManyCases()
    {
        $passingCase = 'I should not case failure';
        $failedCase  = 'I should be triggered';

        self::expectException(Exception::class);
        self::expectExceptionMessage($failedCase);

        $falseValue = false;
        $emptyString = '';
        $zero = 0;

        Shorty::guard()
            ->addCase(true, 'true')
            ->addCase(new \stdClass() instanceof \stdClass, 'instance check')
            ->addCase(isset($emptyString), 'empty strings are going to pass')
            ->addCase(isset($zero), 'zero will pass as only false will fail')
            ->addCase(($falseValue === false), 'expecting false')
            ->addCase((5 < 10), 'Five is less then 10')
            ->addCase((10 < 5), $failedCase)
            ->addCase(function () {
                return true;
            }, 'callback should of return true')
            ->allOrThrow(InvalidArgumentException::class);
    }
}
