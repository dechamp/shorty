FROM php:5.6-apache-jessie

WORKDIR /var/www/html
RUN apt-get update && apt-get install -y git-core wget
RUN cd /tmp && wget https://xdebug.org/files/xdebug-2.5.5.tgz && \
    tar xvzf xdebug-2.5.5.tgz && \
    cd xdebug-2.5.5 && \
    phpize && \
    ./configure --enable-xdebug && \
    make && \
    make install;
RUN echo 'zend_extension="/usr/local/lib/php/extensions/no-debug-non-zts-20131226/xdebug.so"' > /usr/local/etc/php/conf.d/xdebug.ini
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php -r "if (hash_file('SHA384', 'composer-setup.php') === '544e09ee996cdf60ece3804abc52599c22b1f40f4323403c44d44fdfdd586475ca9813a858088ffbc1f233e9b180f061') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
    php composer-setup.php && \
    mv composer.phar /usr/local/bin/composer && \
    php -r "unlink('composer-setup.php');"
RUN composer selfupdate && \
  composer require "phpunit/phpunit:~4.6.6"
RUN composer install
RUN  ln -s /var/www/html/vendor/bin/phpunit /usr/local/bin/phpunit
RUN . ~/.bashrc
