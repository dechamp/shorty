<?php

namespace DeChamp;

use DeChamp\Guard;
use Exception;

class Shorty
{
    const FAILED_TO_FIND_RESULT = "FAILED_TO_FIND_RESULT";

    static public function grabOrNull($haystack, $needle)
    {
        $response = self::find($haystack, $needle);

        return $response !== self::FAILED_TO_FIND_RESULT ? $response : null;
    }

    static public function grabOr($haystack, $needle, $default)
    {
        $response = self::find($haystack, $needle);

        return $response !== self::FAILED_TO_FIND_RESULT ? $response : $default;
    }

    static public function grabOrThrow($haystack, $needle, Exception $exception)
    {
        $response = self::find($haystack, $needle);

        if ($response === self::FAILED_TO_FIND_RESULT) {
            throw $exception;
        }

        return $response;
    }

    static private function find($haystack, $needle)
    {
        if (strpos($needle, "/")) {
            $needles = explode("/", $needle);

            return self::findRecursive($haystack, $needles);
        }


        return self::findOrFail($haystack, $needle);
    }

    static private function findRecursive($haystack, $needles)
    {
        $result = $haystack;

        foreach ($needles as $needle) {
            $result = self::findOrFail($result, $needle);

            if ($result === self::FAILED_TO_FIND_RESULT) {
                return self::FAILED_TO_FIND_RESULT;
            }
        }

        return $result;
    }

    static private function findOrFail($haystack, $needle)
    {
        if (is_object($haystack) && $haystack instanceof \Traversable) {
            $haystack = iterator_to_array($haystack, true);
        }

        $handleArray = is_array($haystack) && array_key_exists($needle, $haystack);

        if ($handleArray) {
            return $haystack[$needle];
        }

        $doObject = is_object($haystack)
            && in_array($needle, array_keys(get_object_vars($haystack)));

        if ($doObject) {
            return $haystack->{$needle};
        }

        return self::FAILED_TO_FIND_RESULT;
    }

    /**
     * @deprecated You should choose to use https://packagist.org/packages/webmozart/assert instead, it's much better at guarding
     *
     * @return \DeChamp\Guard
     */
    static public function guard()
    {
        return new Guard();
    }
}
