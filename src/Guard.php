<?php

namespace DeChamp;

use Exception;
use InvalidArgumentException;

/**
 * Class Guard
 *
 * @deprecated You should choose to use https://packagist.org/packages/webmozart/assert instead, it's much better at guarding
 * @package DeChamp
 */
class Guard
{
    const ERR_MSG_MISSING_CASES = 'at least 1 case is required, please add one using addCase($case, $response)';
    const ERR_MSG_INVALID_TEST_RETURN_TYPE = 'test argument must result in a bool value, you passed "%s" with type of "%s"';
    const ERR_MSG_INVALID_CASE_RESPONSE_ERROR = 'response argument must be a valid string';

    private $cases;

    public function addCase($test, $response)
    {
        if (is_callable($test)) {
            $test = $test();
        }

        $this->guardTestProp($test);
        $this->guardResponseProp($response);

        $this->cases[] = [(bool) $test, $response];

        return $this;
    }

    public function allOrThrow($exception)
    {
        $this->guardCasesProp();

        foreach ($this->cases as $case) {
            $this->checkCase($exception, $case);
        }
    }

    public function throwException($class, $error)
    {
        throw new $class($error);
    }

    private function guardCasesProp()
    {
        if (count($this->cases) < 1) {
            throw new Exception(self::ERR_MSG_MISSING_CASES);
        }
    }

    private function guardTestProp($test)
    {
        if (! is_bool($test)) {
            throw new InvalidArgumentException(
                sprintf(
                    self::ERR_MSG_INVALID_TEST_RETURN_TYPE,
                    $test,
                    gettype($test)
                )
            );
        }
    }

    private function guardResponseProp($response)
    {
        if (! is_string($response) || strlen($response) < 1) {
            throw new InvalidArgumentException(self::ERR_MSG_INVALID_CASE_RESPONSE_ERROR);
        }
    }

    /**
     * @param $exception
     * @param $case
     */
    private function checkCase($exception, $case)
    {
        if ($case[0] === false) {
            return $this->throwException($exception, $case[1]);
        }
    }
}
